"""
=== CIS 322 Project 2 ===
Author: Kayla Walker

Use: Simple server implementation using Flask

"""

from flask import Flask
from flask import render_template

app = Flask(__name__)


@app.route("/<path:path>")
def find_file(path):
    if ("//" in path) or (".." in path) or ("~" in path):
        return error_403(403)
    else:
        try:
            return render_template(path), 200
        except OSError as error:
            return error_404(404)


@app.errorhandler(403)
def error_403(error):
    return render_template("403.html"), 403


@app.errorhandler(404)
def error_404(error):
    return render_template("404.html"), 404


if __name__ == "__main__":
    app.run(debug=True,host='0.0.0.0')
