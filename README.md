# README #

## Author: Kayla Walker, kwalker3@uoregon.edu ##

Repository: bitbucket.org/kwalker3/kwalker3-proj2-pageserver

Basic pageserver using Flask and Docker for CIS 322 at the University of Oregon

### Usage ###

$ ./run.sh

Then launch http://localhost:5000 using web browser, followed by /<filename>

If file exists, server transmits 200/0K followed by file. If file not
found, displays 404 error. If forbidden characters in filepath ("//", "..", or "~"),
displays 403 error.

